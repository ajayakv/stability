# Stability
Stability dotnet core library to provide Resiliency pattern for .NET application.
# Features!
- Circuit Breaker
- Retry Pattern


### Installation
It's available via [a nuget package](https://www.nuget.org/packages/Stability.Net)  
PM> `Install-Package Stability.Net`

## Example : Simple Circuit Breaker
```cssharp
    public async Task Success_with_Close_State() {
        var builder = new CircuitBreakerBuilder();
        var userEntity = new UserEntity();
        var command = builder.AsKey(nameof(userEntity.GetUsers)).WithMaxFailure(3).Build();
        var users = await command.ExecuteAsync(() => userEntity.GetUsers(0), CancellationToken.None);
    }
```
## Example : Circuit Breaker with Fallback
```cssharp
    public async Task Given_Failed_Request_With_Fallback_Should_Execute_Fallback_with_Expected_Result() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();

            CircuitBreakerCommand command = builder.AsKey(nameof(userEntity.GetUsers)).WithMaxFailure(1).WithResetTimeout(TimeSpan.FromSeconds(1)).Build();
            var users = await command.ExecuteAsync(() => userEntity.GetUsers(10), CancellationToken.None, () => userEntity.GetSuccessResult());
        }
```      

## Example : Circuit Breaker with Metrics Collector
```csharp
// Initialize the MetricsCollector Advance uses with Metrics Collector

      public static class MetricsCollector {
        public static IList<CircuitBreakerMetrics> MetricsStore = new List<CircuitBreakerMetrics>();

        public static void SubscribeMetrics() {
            CircuitBreakerBuilder.Feed.Subscribe(metrics => MetricsStore.Add(metrics));
        }
        public static ICircuitBreakerBuilder CreateCommand(string commandName) {
            var builder = new CircuitBreakerBuilder()
                .AsKey(commandName)
                .WithMaxFailure(3)
                .WithResetTimeout(TimeSpan.FromSeconds(10))
                .WithMaxRetry(3)
                .RetryInterval(new List<TimeSpan>()
                {
                    TimeSpan.FromMilliseconds(100),
                    TimeSpan.FromMilliseconds(500),
                    TimeSpan.FromMilliseconds(1000)
                });

            return builder;
        }
    }

// wrap methods calls
      public async Task<IActionResult> GetCircuitBreaker(string simulate, CancellationToken cancellationToken) {
            var person = new Person();
            var builder = MetricsCollector.CreateCommand("GetEmployee");
            CircuitBreakerCommand command = builder.Build();
            IEnumerable<EmployeeModel> result = null;
            try {
                result = await command.ExecuteAsync(() => person.EmployeeModels(simulate, person), cancellationToken);
            }
            catch (CircuitBreakerOpen e) {
                return NotFound(new { CircuitBreakerBuilder.Metrics, Exception = e });
            }
            catch (Exception) {

            }
            return Ok(new { CommandMetrics = command.Metrics, Data = result });
        }
    // Access Metrics
     public IActionResult GetMetrics() {
            return Ok(MetricsCollector.MetricsStore);
        }
 
```