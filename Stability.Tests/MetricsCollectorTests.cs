﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using FluentAssertions;
using Newtonsoft.Json;
using Stability.Enums;
using Stability.Metrics;

namespace Stability.Tests {
    public class MetricsCollectorTests {
        private MetricsProvider CreateNewCollector() {
            return new MetricsProvider();
        }

        [Fact]
        public void Add_Should_Publish_Metrics() {

            var collector = CreateNewCollector();
            var actual = new List<MetricsModel>();

            collector.Subscribe(m => {
                actual.Add(m);
                Console.WriteLine($"{m.Name},{m.CircuitState},{m.SuccessCount}");
            });

            var metric = new CircuitBreakerMetrics {
                Name = "1",
                State = CircuitBreakerState.Open
            };

            collector.Add(metric);
        
            metric.Name = "2";
            metric.Success.Increment();
            metric.State = CircuitBreakerState.HalfOpen;
            collector.Add(metric);
            metric.Name = "3";
            metric.State = CircuitBreakerState.Open;
            collector.Add(metric);
            metric.Name = "4";
            metric.State = CircuitBreakerState.Closed;
            collector.Add(metric);

            actual.Count.ShouldBeEquivalentTo(4);

            actual.First().Name.ShouldBeEquivalentTo(1);
            actual.First().CircuitState.ShouldBeEquivalentTo("Open");
            actual.First().SuccessCount.ShouldBeEquivalentTo(0);

            actual.Last().Name.ShouldBeEquivalentTo(4);
            actual.Last().CircuitState.ShouldBeEquivalentTo("Closed");
            actual.Last().SuccessCount.ShouldBeEquivalentTo(1);

        }
    }
}
