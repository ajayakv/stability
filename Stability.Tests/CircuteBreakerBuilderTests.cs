﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Newtonsoft.Json;
using NSubstitute;
using Stability.Commands;
using Stability.Enums;
using Stability.Exceptions;
using Stability.Interfaces;
using Stability.Metrics;
using Stability.Tests.Models;
using Xunit;

namespace Stability.Tests {
    public class CircuitBreakerBuilderTests {
        private readonly ISystemClockProvider _clockProvider;
        private readonly DateTime _currentDateTime = new DateTime(2017, 1, 1, 1, 0, 0, DateTimeKind.Utc);
        public CircuitBreakerBuilderTests() {
            _clockProvider = Substitute.For<ISystemClockProvider>();
            _clockProvider.GetUtcNow().Returns(_currentDateTime);
            CircuitBreakerBuilder.ResetMetrics();
        }

        public ICircuitBreakerBuilder CreateBuilder() {
            return new CircuitBreakerBuilder(_clockProvider);
        }

        [Fact]
        public async Task SuccessTest_with_Close_State() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();
            var command = builder.AsKey("http-rest").Build();
            var actual = await command.ExecuteAsync(() => userEntity.GetUsers(0), CancellationToken.None);
            actual.Should().NotBeNull();
            command.Metrics.State.Should().Be(CircuitBreakerState.Closed);
            command.Metrics.CommandType.Should().Be("CircuitBreakerCommand");
        }

        [Fact]
        public async Task Failed_should_Open_Circuit() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();
            CircuitBreakerCommand command = builder.AsKey("http-rest").Build();
            await Assert.ThrowsAsync<Exception>(() => command.ExecuteAsync(() => userEntity.GetUsers(1), CancellationToken.None));
            command.Metrics.State.Should().Be(CircuitBreakerState.Open);
        }

        [Fact]
        public async Task Given_Threshold_MaxFailures_Reached_Should_Open_Circuit() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();
            //1st attempt:Open
            CircuitBreakerCommand command = builder.AsKey(nameof(userEntity.GetUsers)).WithMaxFailure(3).Build();
            await Assert.ThrowsAsync<Exception>(() => command.ExecuteAsync(() => userEntity.GetUsers(10), CancellationToken.None));
            command.Metrics.Failed.Count.Should().Be(1);
            command.Metrics.State.Should().Be(CircuitBreakerState.Closed);

            //2nd attempt:Open
            await Assert.ThrowsAsync<Exception>(() => command.ExecuteAsync(() => userEntity.GetUsers(10), CancellationToken.None));
            command.Metrics.Failed.Count.Should().Be(2);
            command.Metrics.State.Should().Be(CircuitBreakerState.Closed);

            //3rd attempt:Closed
            await Assert.ThrowsAsync<Exception>(() => command.ExecuteAsync(() => userEntity.GetUsers(10), CancellationToken.None));
            command.Metrics.Failed.Count.Should().Be(3);
            command.Metrics.State.Should().Be(CircuitBreakerState.Open);
        }

        [Fact]
        public async Task Given_Threshold_ResetTimeout_Elapsed_Should_HalfOpen_Circuit() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();
            var resetTimeout = TimeSpan.FromSeconds(10);

            var command = builder.AsKey(nameof(userEntity.GetUsers)).WithMaxFailure(3).WithResetTimeout(resetTimeout).Build();
            command.Subscribe(metrics => {
                Console.WriteLine($"metrics.State={metrics.CircuitState}");
            }
            );

            await Assert.ThrowsAsync<Exception>(() => command.ExecuteAsync(() => userEntity.GetUsers(10), CancellationToken.None));
            await Assert.ThrowsAsync<Exception>(() => command.ExecuteAsync(() => userEntity.GetUsers(10), CancellationToken.None));
            await Assert.ThrowsAsync<Exception>(() => command.ExecuteAsync(() => userEntity.GetUsers(10), CancellationToken.None));

            _clockProvider.GetUtcNow().Returns(_currentDateTime + TimeSpan.FromSeconds(5));
            _clockProvider.GetUtcNow().Second.ShouldBeEquivalentTo(5);

            await Assert.ThrowsAsync<CircuitBreakerOpen>(() => command.ExecuteAsync(() => userEntity.GetUsers(10), CancellationToken.None));

            _clockProvider.GetUtcNow().Returns(_currentDateTime + resetTimeout);

            await Assert.ThrowsAsync<Exception>(() => command.ExecuteAsync(() => userEntity.GetUsers(10), CancellationToken.None));
            command.Metrics.State.Should().Be(CircuitBreakerState.Open);

        }

        [Fact]
        public async Task Given_Success_Request_After_HalfOpen_Should_Close_Circuit() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();
            var resetTimeout = TimeSpan.FromMilliseconds(500);

            CircuitBreakerCommand command = builder.AsKey(nameof(userEntity.GetUsers)).WithMaxFailure(2).WithResetTimeout(resetTimeout).Build();

            await Assert.ThrowsAsync<Exception>(() => command.ExecuteAsync(() => userEntity.GetUsers(5), CancellationToken.None));
            await Assert.ThrowsAsync<Exception>(() => command.ExecuteAsync(() => userEntity.GetUsers(5), CancellationToken.None));
            command.Metrics.State.Should().Be(CircuitBreakerState.Open);
            //set reset timeout to expired
            _clockProvider.GetUtcNow().Returns(_clockProvider.GetUtcNow() + resetTimeout);

            await command.ExecuteAsync(() => userEntity.GetSuccessResult(), CancellationToken.None);
            command.Metrics.State.Should().Be(CircuitBreakerState.Closed);
        }

        [Fact]
        public async Task Given_Request_When_Circuit_Open_Should_Throw_CircuitBreaker_Open_Exception_fail_fast() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();
            //1st attempt:Open
            CircuitBreakerCommand command = builder.AsKey(nameof(userEntity.GetUsers)).WithMaxFailure(1).WithResetTimeout(TimeSpan.FromSeconds(1)).Build();
            await Assert.ThrowsAsync<Exception>(() => command.ExecuteAsync(() => userEntity.GetUsers(10), CancellationToken.None));
            command.Metrics.State.Should().Be(CircuitBreakerState.Open);

            await Assert.ThrowsAsync<CircuitBreakerOpen>(() => command.ExecuteAsync(() => userEntity.GetSuccessResult(), CancellationToken.None));

            command.Metrics.State.Should().Be(CircuitBreakerState.Open);
        }


        [Fact]
        public async Task Success_with_Obserbable_Should_Returns_Metrics_Changes_DataPoints() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();
            CircuitBreakerCommand command =
                builder.AsKey("http-rest")
                .Subscribe(
                    metrics => Console.WriteLine(JsonConvert.SerializeObject(metrics))
                    ).Build();

            var actual = await command.ExecuteAsync(() => userEntity.GetUsers(0), CancellationToken.None);
            actual.Should().NotBeNull();
            command.Metrics.State.Should().Be(CircuitBreakerState.Closed);
        }

        [Fact]
        public async Task Given_Cancellation_as_Candel_Should_Throw_CircuitBreaker_Timeout_Exception() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();
            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            var command = builder.AsKey(nameof(userEntity.GetUsers)).WithTimeout(TimeSpan.FromMilliseconds(300)).Build();
            tokenSource.Cancel();
            var actual = await Assert.ThrowsAsync<CircuitBreakerTimeout>(() => command.ExecuteAsync(() => userEntity.GetDelaySuccessResult(TimeSpan.FromSeconds(1)), token));

            actual.Message.ShouldBeEquivalentTo("Task execution took longer than timeout:[300],Exception:The operation was canceled.");
        }

        [Fact]
        public async Task Given_Request_delay_WithTimeout_Should_Throw_CircuitBreaker_Timeout_Exception() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();
            var command = builder.AsKey(nameof(userEntity.GetUsers)).WithTimeout(TimeSpan.FromMilliseconds(100)).WithMaxFailure(1).Build();

            var actual = await Assert.ThrowsAsync<CircuitBreakerTimeout>(() => command.ExecuteAsync(() => userEntity.GetDelaySuccessResult(TimeSpan.FromMilliseconds(200)), CancellationToken.None));
            actual.Message.Should().Contain("Task execution took [");
            actual.Message.Should().Contain("ms] longer than timeout [100ms]");
            command.Metrics.Failed.Count.ShouldBeEquivalentTo(1);
            command.Metrics.LastException.Message.Should().Contain("longer than timeout [100ms]");
        }

        [Fact]
        public async Task Given_Request_delay_LessThan_Timeout_Should_Returns_Expected_Results() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();

            CircuitBreakerCommand command = builder.AsKey(nameof(userEntity.GetUsers)).WithTimeout(TimeSpan.FromSeconds(1)).Build();
            var actual = await command.ExecuteAsync(() => userEntity.GetDelaySuccessResult(TimeSpan.FromMilliseconds(500)), CancellationToken.None);

            actual.Should().NotBeNull();
            actual.page.ShouldBeEquivalentTo(1);
        }

        [Fact]
        public async Task Given_Failed_Request_With_Fallback_Should_Execute_Fallback_with_Expected_Result() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();

            CircuitBreakerCommand command = builder.AsKey(nameof(userEntity.GetUsers)).WithMaxFailure(1).WithResetTimeout(TimeSpan.FromSeconds(1)).Build();

            var actual = await command.ExecuteAsync(() => userEntity.GetUsers(10), CancellationToken.None, () => userEntity.GetSuccessResult());
            actual.Should().NotBeNull();
            command.Metrics.State.Should().Be(CircuitBreakerState.Open);
            command.Metrics.Failed.Count.ShouldBeEquivalentTo(1);
            command.Metrics.FallBack.Count.ShouldBeEquivalentTo(1);
        }

        [Fact]
        public void Given_Request_Parallel_Should_Expected_Correct_Metrics_Result() {
            var builder = CreateBuilder();
            var userEntity = new UserEntity();

            Parallel.For(1, 10, async (i, state) => {
                var cmd = builder.AsKey(nameof(userEntity.GetUsers)).WithMaxFailure(1).WithResetTimeout(TimeSpan.FromSeconds(1)).Build();
                await cmd.ExecuteAsync(() => userEntity.GetSuccessResult(), CancellationToken.None);
                Console.WriteLine($"{i} = {cmd.Metrics.Success.Count}");
            });

            var command = builder.AsKey(nameof(userEntity.GetUsers)).WithMaxFailure(1).WithResetTimeout(TimeSpan.FromSeconds(1)).Build();
            command.Metrics.Success.Count.ShouldBeEquivalentTo(9);
        }

        [Fact]
        public void Given_Subscribe_Should_Expected_Correct_Metrics_Result() {
            var actual = new List<MetricsModel>();
            CircuitBreakerBuilder.Feed.Subscribe(model => {
                Console.WriteLine($"Feed Test{model}");
                actual.Add(model);
            });

            var userEntity = new UserEntity();
            var builder = CreateBuilder();
            Parallel.For(0, 10, async (i, state) => {
                var cmd = builder.AsKey(nameof(userEntity.GetUsers)).WithMaxFailure(1).WithResetTimeout(TimeSpan.FromSeconds(1)).Build();
                cmd.Subscribe(model => {
                    //Console.WriteLine(i);
                    actual.Add(model);
                });
                await cmd.ExecuteAsync(() => userEntity.GetSuccessResult(), CancellationToken.None);
            });


            actual.Count.ShouldBeEquivalentTo(20);
        }
    }
}
