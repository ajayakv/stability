using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Newtonsoft.Json;
using Stability.Commands;
using Stability.Metrics;
using Stability.Tests.Models;
using Xunit;

namespace Stability.Tests {
    public class RetryCommandTests {
        public RetryCommandTests() {
            CircuitBreakerBuilder.ResetMetrics();
        }
        [Fact]
        public async Task ExecuteAsync_Given_Success_Should_Returns_expected_Result() {

            var userEntity = new UserEntity();

            RetryCommand command = new CircuitBreakerBuilder().AsKey("http-request").BuildWithRetry();

            var actual = await command.ExecuteAsync(() => userEntity.GetUsers(0), CancellationToken.None);
            actual.page.Should().Be(1);
            actual.data.Should().NotBeNull();
            command.Metrics.CommandType.Should().Be("RetryCommand");
            command.Metrics.Success.Count.Should().Be(1);
        }

        // [Fact]
        public async Task ExecuteAsync_Given_MaxRetry_with_interval_Should_Attempt() {
            var mertics = new List<MetricsModel>();
            RetryCommand command = new CircuitBreakerBuilder().AsKey("http-request").WithMaxRetry(3).RetryInterval(
                new List<TimeSpan>()
                {
                    TimeSpan.FromMilliseconds(100),
                    TimeSpan.FromMilliseconds(200),
                    TimeSpan.FromMilliseconds(300)
                }).Subscribe(metrics => mertics.Add(metrics)).BuildWithRetry();

            var userEntity = new UserEntity();

            var actual = await command.ExecuteAsync(() => userEntity.GetUsers(3), CancellationToken.None);

            actual.page.Should().Be(1);
            actual.data.Should().NotBeNull();

            command.Metrics.Retry.Count.Should().Be(3);
            command.Metrics.Failed.Count.Should().Be(3);

            CircuitBreakerBuilder.Metrics.Count.CompareTo(1);
            CircuitBreakerBuilder.Metrics.ContainsKey("http-request.RetryCommand").Should().Be(true);

            mertics.Count.Should().Be(1);
        }

        [Fact]
        public async Task WithObservableTest() {
            var userEntity = new UserEntity();
            var mertics = new List<MetricsModel>();
            var builder = new CircuitBreakerBuilder();

            RetryCommand command = builder.AsKey("http-request").Subscribe(
                metrics => {
                    mertics.Add(metrics);
                }
                ).BuildWithRetry();

            var actual = await command.ExecuteAsync(() => userEntity.GetUsers(0), CancellationToken.None);
            actual.page.Should().Be(1);
            actual.data.Should().NotBeNull();
            mertics.Count.Should().Be(1);
        }
    }

}
