using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Stability.Tests.Models {
    public class UserEntity {
        private readonly HttpClient _httpClient;
        const string SuccessJson = "{\"page\":1,\"per_page\":3,\"total\":12,\"total_pages\":4,\"data\":[{\"id\":1,\"first_name\":\"George\",\"last_name\":\"Bluth\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/calebogden/128.jpg\"},{\"id\":2,\"first_name\":\"Janet\",\"last_name\":\"Weaver\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg\"},{\"id\":3,\"first_name\":\"Emma\",\"last_name\":\"Wong\",\"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/olegpogodaev/128.jpg\"}]}";
        
        private int _executeCount = 0;
        public UserEntity() {
            _httpClient = new HttpClient { BaseAddress = new Uri("https://reqres.in") };
        }
        
        public Task<UsersResponseModels> GetSuccessResult() {
            var result = JsonConvert.DeserializeObject<UsersResponseModels>(SuccessJson);
            return Task.FromResult(result);
        }

        public async Task<UsersResponseModels> GetDelaySuccessResult(TimeSpan delay) {
            await Task.Delay(delay);
            var result = JsonConvert.DeserializeObject<UsersResponseModels>(SuccessJson);
            return result;
        }

        public async Task<UsersResponseModels> GetUsers(int succeedOnAttemptCountOf) {

            //var result = await _httpClient.GetAsync("/api/users?page=1");
            //result.EnsureSuccessStatusCode();
            if (succeedOnAttemptCountOf == _executeCount) {
                var result = new StringContent(SuccessJson);

                var jsonResult = await result.ReadAsStringAsync();
                //Console.WriteLine(jsonResult);
                _executeCount++;
                return JsonConvert.DeserializeObject<UsersResponseModels>(jsonResult);
            }
            _executeCount++;
            throw new Exception("failed to execute GetUsers()");
        }

    }
}