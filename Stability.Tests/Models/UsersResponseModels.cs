using System.Collections.Generic;

namespace Stability.Tests.Models
{
    public class UsersResponseModels {
        public int page { get; set; }
        public int per_page { get; set; }
        public int total { get; set; }
        public int total_pages { get; set; }
        public List<Datum> data { get; set; }
    }
}