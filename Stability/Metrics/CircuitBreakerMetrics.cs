using System;
using System.Threading;
using Stability.Enums;

namespace Stability.Metrics {
    public class ThresholdModel {
        public int MaxFailures { get; set; }
        public TimeSpan ResetTimeout { get; set; }
        public TimeSpan Timeout { get; set; }
    }

    public class CounterModel {
        public int Count;
        public CounterModel() {
            Interlocked.Exchange(ref Count, 0);
        }
        public void Increment() {
            Interlocked.Increment(ref Count);
        }
        public void Reset() {
            Interlocked.Exchange(ref Count, 0);
        }
    }

    public class MetricsModel {
        public string Name { get; }
        public string CommandType { get; }

        public string CircuitState { get; }

        public long LatencyInMilliseconds { get; }
        public string LastException { get; }
        public DateTime LastStatusChangedTime { get; }
        public ThresholdModel Threshold { get; }

        public long SuccessCount { get; }
        public long FailedCount { get; }
        public long RetryCount { get; }
        public long FallBackCount { get; }
        public MetricsModel(CircuitBreakerMetrics metrics) {
            Name = metrics.Name;
            CommandType = metrics.CommandType;
            CircuitState = metrics.State.ToString("G");
            LatencyInMilliseconds = metrics.LatencyInMilliseconds;
            LastException = metrics?.LastException?.Message;
            LastStatusChangedTime = metrics.LastStatusChangedTime;
            Threshold = metrics.Threshold;
            SuccessCount = metrics.Success.Count;
            FailedCount = metrics.Failed.Count;
            RetryCount = metrics.Retry.Count;
            FallBackCount = metrics.FallBack.Count;
        }

        public override string ToString() {
            return $"{Name}[{CircuitState}][{LatencyInMilliseconds}ms]";
        }
    }

    public class CircuitBreakerMetrics {
        public readonly CounterModel Success;
        public readonly CounterModel Failed;
        public readonly CounterModel Retry;
        public readonly CounterModel FallBack;

        public string Name { get; set; }
        public string CommandType { get; set; }

        public string CircuitState => State.ToString("G");

        public long LatencyInMilliseconds { get; set; }
        public Exception LastException { get; set; }
        public CircuitBreakerState State { get; set; }
        public DateTime LastStatusChangedTime { get; set; }
        public ThresholdModel Threshold { get; set; }

        public CircuitBreakerMetrics() {
            Success = new CounterModel();
            Failed = new CounterModel();
            Retry = new CounterModel();
            FallBack = new CounterModel();
        }

        public void ResetCounters() {
            Success.Reset();
            Failed.Reset();
            Retry.Reset();
            FallBack.Reset();
        }
    }
}