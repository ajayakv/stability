using System;

namespace Stability.Exceptions
{
    public class CircuitBreakerTimeout : Exception {
        public CircuitBreakerTimeout(string message) : base(message) {
        }
    }
}