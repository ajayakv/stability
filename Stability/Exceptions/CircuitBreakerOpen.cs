using System;

namespace Stability.Exceptions
{
    public class CircuitBreakerOpen : Exception {

        public CircuitBreakerOpen(Exception lastException) : base(lastException.Message) {
        }
    }
}