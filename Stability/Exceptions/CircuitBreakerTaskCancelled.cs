using System;

namespace Stability.Exceptions {
    public class CircuitBreakerTaskCancelled : Exception {
        public CircuitBreakerTaskCancelled(string message) : base(message) {
        }
    }
}