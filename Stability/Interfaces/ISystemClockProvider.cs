﻿using System;

namespace Stability.Interfaces
{
    public interface ISystemClockProvider {
        DateTime GetUtcNow();
        DateTime AddTick(TimeSpan span);
    }
}