using System;
using System.Collections.Generic;
using System.Reactive.Subjects;
using Stability.Commands;
using Stability.Metrics;

namespace Stability.Interfaces {
    public interface ICircuitBreakerBuilder {
        ICircuitBreakerBuilder AsKey(string metricsKey);
        ICircuitBreakerBuilder RetryInterval(IEnumerable<TimeSpan> retryTimeSpans);
        ICircuitBreakerBuilder WithMaxRetry(int maxRetries);
        ICircuitBreakerBuilder WithMaxFailure(int failureCount);
        ICircuitBreakerBuilder WithResetTimeout(TimeSpan resetTimeout);
        ICircuitBreakerBuilder WithTimeout(TimeSpan timeout);
        ICircuitBreakerBuilder Subscribe(Action<MetricsModel> action);
        string GetMetricsKey(string commandType);
        ThresholdModel Threshold { get; }
        int MaxRetries { get; }
        IEnumerable<TimeSpan> RetryTimeSpans { get; }

        CircuitBreakerCommand Build();
        RetryCommand BuildWithRetry();
    }
}