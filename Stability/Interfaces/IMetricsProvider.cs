﻿using System;
using System.Reactive.Subjects;
using Stability.Metrics;

namespace Stability.Interfaces
{
    public interface IMetricsProvider {
        Subject<MetricsModel> GetSubject();
        IDisposable Subscribe(Action<MetricsModel> subscriber);
        void Add(CircuitBreakerMetrics metrics);
        void Complete();
    }
}