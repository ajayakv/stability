using System;
using System.Diagnostics;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using Stability.Enums;
using Stability.Interfaces;
using Stability.Metrics;
using Stability.Exceptions;

namespace Stability.Implements {

    public abstract class CircuitBreakerBase : IDisposable {
        private readonly CircuitBreakerMetrics _metrics;
        private readonly ISystemClockProvider _clockProvider;
        private readonly IMetricsProvider _metricsProvider;
        public CircuitBreakerMetrics Metrics => _metrics;
        protected CircuitBreakerBase(CircuitBreakerMetrics metrics, ISystemClockProvider clockProvider, IMetricsProvider metricsProvider) {
            _metrics = metrics;
            _clockProvider = clockProvider;
            _metricsProvider = metricsProvider;
        }

        private IDisposable _disposable;
        public void Subscribe(Action<MetricsModel> action) {
            _disposable?.Dispose();
            _disposable = _metricsProvider.Subscribe(action);
        }

        protected bool IsCircuitOpen() {
            if (_metrics.State != CircuitBreakerState.Open) return false;

            if (!IsResetTimeoutExpired()) return true;

            _metrics.State = CircuitBreakerState.HalfOpen;
            return false;
        }
        protected Exception GetLastException() {
            return _metrics.LastException;
        }

        private bool IsResetTimeoutExpired() {

            if (_metrics.LastStatusChangedTime + _metrics.Threshold.ResetTimeout <= _clockProvider.GetUtcNow()) {
                return true;
            }

            return false;
        }
        private void Trip() {
            if (_metrics.State == default(CircuitBreakerState) && _metrics.State != CircuitBreakerState.Open) return;
            _metrics.State = CircuitBreakerState.Open;
            OnMetricsChanged();
        }

        private void Reset() {
            _metrics.State = CircuitBreakerState.Closed;
            _metrics.ResetCounters();
            _metrics.LastException = null;
            _metrics.LatencyInMilliseconds = 0;
            _metrics.LastStatusChangedTime = _clockProvider.GetUtcNow();
        }

        protected void OnSuccess(Stopwatch stopwatch) {

            if (_metrics.State == CircuitBreakerState.HalfOpen) {
                Reset();
            }
            _metrics.LatencyInMilliseconds = stopwatch.ElapsedMilliseconds;
            _metrics.Success.Increment();
            OnMetricsChanged();
        }

        protected void OnMetricsChanged() {
            _metrics.LastStatusChangedTime = _clockProvider.GetUtcNow();
            _metricsProvider.Add(_metrics);
        }

        protected void OnFailed(Exception e, Stopwatch stopwatch) {
            _metrics.LastException = e;
            _metrics.LatencyInMilliseconds = stopwatch.ElapsedMilliseconds;
            _metrics.Failed.Increment();

            if (_metrics.Threshold.MaxFailures <= _metrics.Failed.Count) {
                Trip();
            }
            else {
                OnMetricsChanged();
            }
        }

        protected void OnFallback(Stopwatch stopwatch) {
            _metrics.LatencyInMilliseconds = stopwatch.ElapsedMilliseconds;
            _metrics.FallBack.Increment();
            OnMetricsChanged();
        }

        private async Task<T> InvokeAsyncWithTimeout<T>(Func<Task<T>> p, CancellationToken cancellationToken) {

            if (_metrics.Threshold.Timeout == default(TimeSpan)) {
                return await p.Invoke();
            }

            try {

                cancellationToken.ThrowIfCancellationRequested();

                return await p.Invoke();
            }
            catch (Exception e) {
                if (cancellationToken.IsCancellationRequested) {
                    throw new CircuitBreakerTimeout($"Task execution took longer than timeout:[{_metrics.Threshold.Timeout.Milliseconds}],Exception:{e.Message}");
                }
                throw;
            }
        }

        internal async Task<T> InvokeAsync<T>(Func<Task<T>> p, CancellationToken cancellationToken, Func<Task<T>> fallBackAction) {

            var timeMe = Stopwatch.StartNew();
            timeMe.Start();
            try {
                var result = await InvokeAsyncWithTimeout(p, cancellationToken);
                timeMe.Stop();
                OnSuccess(timeMe);

                return result;
            }
            catch (Exception e) {
                timeMe.Stop();
                OnFailed(e, timeMe);
                if (fallBackAction != null) {
                    return await ExecuteFallbackAsync(fallBackAction, cancellationToken);
                }

                throw;
            }
            finally {
                ThrowIfTimeoutExpired(timeMe);
            }
        }

        private void ThrowIfTimeoutExpired(Stopwatch timeMe) {
            if (_metrics.Threshold.Timeout != default(TimeSpan) &&
                timeMe.ElapsedMilliseconds > _metrics.Threshold.Timeout.Milliseconds) {
                var timeoutException = new CircuitBreakerTimeout(
                    $"Task execution took [{timeMe.ElapsedMilliseconds}ms] longer than timeout [{_metrics.Threshold.Timeout.Milliseconds}ms]");
                OnFailed(timeoutException, timeMe);

                if (_metrics.Threshold.MaxFailures != default(int) && _metrics.Threshold.MaxFailures <= _metrics.Failed.Count) {
                    throw timeoutException;
                }
            }
        }

        public abstract Task<T> ExecuteAsync<T>(Func<Task<T>> primaryAction, CancellationToken cancellationToken, Func<Task<T>> fallBackAction = null);

        public async Task<T> ExecuteFallbackAsync<T>(Func<Task<T>> p, CancellationToken cancellationToken) {
            var timeMe = Stopwatch.StartNew();
            timeMe.Start();
            var result = await InvokeAsyncWithTimeout(p, cancellationToken);
            timeMe.Stop();
            OnFallback(timeMe);
            return result;
        }

        public void Dispose() {
            _disposable?.Dispose();
            Console.WriteLine("Command : Disposed");
        }
    }
}