﻿using System;
using Stability.Interfaces;

namespace Stability.Implements {
    public class SystemClockProvider : ISystemClockProvider {
        public DateTime GetUtcNow() {
            return DateTime.UtcNow;
        }

        public DateTime AddTick(TimeSpan span) {
            return GetUtcNow() + span;
        }
    }
}
