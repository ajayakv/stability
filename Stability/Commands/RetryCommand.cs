using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using Stability.Implements;
using Stability.Interfaces;
using Stability.Metrics;

namespace Stability.Commands {
    public sealed class RetryCommand : CircuitBreakerBase {
        private readonly int _maxRetryCount;
        private readonly IEnumerator<TimeSpan> _delayPerAttempt;
        private readonly IList<TimeSpan> _defaultPerAttempt = new List<TimeSpan>() { TimeSpan.FromMilliseconds(100) };

        public RetryCommand(CircuitBreakerMetrics metrics, ISystemClockProvider clockProvider, int maxRetryCount, IMetricsProvider metricsProvider, IEnumerable<TimeSpan> delayPerAttempt) : base(metrics, clockProvider, metricsProvider) {
            _maxRetryCount = maxRetryCount;
            _delayPerAttempt = delayPerAttempt == null ? _defaultPerAttempt.GetEnumerator() : delayPerAttempt.GetEnumerator();
        }

        public override async Task<T> ExecuteAsync<T>(Func<Task<T>> primaryAction, CancellationToken tokenSource, Func<Task<T>> fallBackAction = null) {

            var count = Enumerable.Range(1, _maxRetryCount).GetEnumerator();
            _delayPerAttempt.Reset();
            var currentDelay = _delayPerAttempt.Current;
            for (; ; )
            {
                try {
                    return await base.InvokeAsync(primaryAction, tokenSource, fallBackAction);
                }
                catch (Exception) {
                    if (!count.MoveNext()) {
                        count.Dispose();
                        throw;
                    }
                    if (_delayPerAttempt.MoveNext()) {
                        currentDelay = _delayPerAttempt.Current;
                    }
                    Metrics.Retry.Increment();
                    OnMetricsChanged();
                    await Task.Delay(currentDelay, tokenSource);
                }
            }
        }
    }
}