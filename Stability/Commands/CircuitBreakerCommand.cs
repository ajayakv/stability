using System;
using System.Threading;
using System.Threading.Tasks;
using Stability.Exceptions;
using Stability.Implements;
using Stability.Interfaces;
using Stability.Metrics;

namespace Stability.Commands {

    public sealed class CircuitBreakerCommand : CircuitBreakerBase {

        public CircuitBreakerCommand(CircuitBreakerMetrics metrics, ISystemClockProvider clockProvider, IMetricsProvider metricsProvider) : base(metrics, clockProvider, metricsProvider) {
        }

        public override Task<T> ExecuteAsync<T>(Func<Task<T>> primaryAction, CancellationToken tokenSource, Func<Task<T>> fallBackAction = null) {

            if (IsCircuitOpen()) {
                throw new CircuitBreakerOpen(GetLastException());
            }

            return base.InvokeAsync(primaryAction, tokenSource,fallBackAction);
        }
         
    }
}