﻿using System;
using System.Reactive.Subjects;
using System.Runtime.CompilerServices;
using Stability.Interfaces;
using Stability.Metrics;

namespace Stability {
    public class MetricsProvider : IMetricsProvider {

        public MetricsProvider() {
            _subject = new Subject<MetricsModel>();
        }

        private readonly Subject<MetricsModel> _subject;
        public Subject<MetricsModel> GetSubject() {
            return _subject;
        }

        public IDisposable Subscribe(Action<MetricsModel> subscriber) {
            return _subject.Subscribe(subscriber);
        }

        public void Add(CircuitBreakerMetrics metrics) {
            var value = new MetricsModel(metrics);
            _subject.OnNext(value);
        }

        public void Complete() {
            _subject.OnCompleted();
        }
    }
}
