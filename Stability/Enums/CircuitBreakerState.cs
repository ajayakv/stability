namespace Stability.Enums
{
    public enum CircuitBreakerState {
        Open,
        Closed,
        HalfOpen
    }
}