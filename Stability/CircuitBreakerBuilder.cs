using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Stability.Commands;
using Stability.Enums;
using Stability.Implements;
using Stability.Interfaces;
using Stability.Metrics;

namespace Stability {
    public sealed class CircuitBreakerBuilder : ICircuitBreakerBuilder {
        private readonly ISystemClockProvider _clockProvider;
        private string _metricsKey;
        private IEnumerable<TimeSpan> _retryTimeSpans;
        private readonly ThresholdModel _threshold = new ThresholdModel();

        public CircuitBreakerBuilder() : this(new SystemClockProvider()) {

        }
        public CircuitBreakerBuilder(ISystemClockProvider clockProvider) {
            _clockProvider = clockProvider;
        }

        public ICircuitBreakerBuilder AsKey(string metricsKey) {
            _metricsKey = metricsKey;
            return this;
        }

        public string GetMetricsKey(string commandType) {
            return $"{_metricsKey}.{commandType}";
        }

        public ThresholdModel Threshold => _threshold;

        public int MaxRetries => _maxRetries;

        public IEnumerable<TimeSpan> RetryTimeSpans => _retryTimeSpans;

        public ICircuitBreakerBuilder RetryInterval(IEnumerable<TimeSpan> retryTimeSpans) {
            _retryTimeSpans = retryTimeSpans;
            return this;
        }

        private int _maxRetries;
        public ICircuitBreakerBuilder WithMaxRetry(int maxRetries) {
            _maxRetries = maxRetries;
            return this;
        }

        public ICircuitBreakerBuilder WithMaxFailure(int failureCount) {
            _threshold.MaxFailures = failureCount;
            return this;
        }

        public ICircuitBreakerBuilder WithResetTimeout(TimeSpan resetTimeout) {
            _threshold.ResetTimeout = resetTimeout;
            return this;
        }

        public ICircuitBreakerBuilder WithTimeout(TimeSpan timeout) {
            _threshold.Timeout = timeout;
            return this;
        }

        private Action<MetricsModel> _subscription;
        public ICircuitBreakerBuilder Subscribe(Action<MetricsModel> action) {
            _subscription = action;
            return this;
        }

        private readonly Object _lockObject = new object();
        private CircuitBreakerMetrics CircuitBreakerMetrics(string commandType) {
            var metricKey = GetMetricsKey(commandType);
            if (Metrics.TryGetValue(metricKey, out CircuitBreakerMetrics metrics)) {
                return metrics;
            }
            lock (_lockObject) {
                if (Metrics.TryGetValue(metricKey, out CircuitBreakerMetrics metrics1)) {
                    return metrics1;
                }
                metrics = new CircuitBreakerMetrics() { State = CircuitBreakerState.Closed, Name = metricKey, Threshold = Threshold, CommandType = commandType };
                Metrics.TryAdd(metricKey, metrics);
                return metrics;
            }
        }

        private static readonly Subject<MetricsModel> MetricsFeed = new  Subject<MetricsModel>();
        public static IObservable<MetricsModel> Feed => MetricsFeed.AsObservable();

        public static ConcurrentDictionary<string, CircuitBreakerMetrics> Metrics { get; private set; } = new ConcurrentDictionary<string, CircuitBreakerMetrics>();
        public static void ResetMetrics() {
            Metrics = new ConcurrentDictionary<string, CircuitBreakerMetrics>();
        }

        public CircuitBreakerCommand Build() {
            var metrics = CircuitBreakerMetrics(nameof(CircuitBreakerCommand));
            var metricsProvider = CreateMetricsProvider();
            var cmd = new CircuitBreakerCommand(metrics, _clockProvider, metricsProvider);
            if (_subscription != null) {
                cmd.Subscribe(_subscription);
            }

            return cmd;
        }

        private static MetricsProvider CreateMetricsProvider() {
            var metricsProvider = new MetricsProvider();
            metricsProvider.Subscribe(model => MetricsFeed.OnNext(model));
            return metricsProvider;
        }

        public RetryCommand BuildWithRetry() {
            var metrics = CircuitBreakerMetrics(nameof(RetryCommand));
            var metricsProvider = CreateMetricsProvider();
            var cmd = new RetryCommand(metrics, _clockProvider, MaxRetries, metricsProvider, RetryTimeSpans);
            if (_subscription != null) {
                cmd.Subscribe(_subscription);
            }
            return cmd;
        }
    }
}