﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sample.Entities {

    public class EmployeeModel {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }

    public class Person {

        public async Task<IEnumerable<EmployeeModel>> GetEmployees() {
            var result = new List<EmployeeModel> { new EmployeeModel() { Id = 1, Name = "ajay kumar", Age = 35 } };
            return await Task.FromResult(result);
        }
        public async Task<IEnumerable<EmployeeModel>> GetEmployeesWithDelay() {
            await Task.Delay(1000);
            return await GetEmployees();
        }
        public Task<IEnumerable<EmployeeModel>> GetEmployeesWithFailed() {
            throw new Exception("Unable to get employee");
        }
        public async Task<IEnumerable<EmployeeModel>> EmployeeModels(string simulate, Person person) {
            IEnumerable<EmployeeModel> result = null;
            switch (simulate) {
                case "failed":
                    result = await person.GetEmployeesWithFailed();
                    break;
                case "delay":
                    result = await person.GetEmployeesWithDelay();
                    break;
                default:
                    result = await person.GetEmployees();
                    break;
            }
            return result;
        }


    }
}
