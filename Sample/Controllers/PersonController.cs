﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sample.Entities;
using Stability;
using Stability.Commands;
using Stability.Exceptions;

namespace Sample.Controllers {
    [Route("api/Person")]
    public class PersonController : Controller {
        public PersonController() {

        }

        [HttpGet("circuitBreaker/{simulate}")]
        public async Task<IActionResult> GetCircuitBreaker(string simulate, CancellationToken cancellationToken) {

            var person = new Person();
            var builder = MetricsCollector.CreateCommand("GetEmployee");
            CircuitBreakerCommand command = builder.Build();
            IEnumerable<EmployeeModel> result = null;
            try {

                result = await command.ExecuteAsync(() => person.EmployeeModels(simulate, person), cancellationToken);
            }
            catch (CircuitBreakerOpen e) {
                return NotFound(new { CircuitBreakerBuilder.Metrics, Exception = e });
            }
            catch (Exception) {

            }
            return Ok(new { CommandMetrics = command.Metrics, Data = result });
        }

        [HttpGet("retry/{simulate}")]
        public async Task<IActionResult> GetRetry(string simulate, CancellationToken cancellationToken) {

            var person = new Person();
            var builder = MetricsCollector.CreateCommand("GetEmployee");
            RetryCommand command = builder.BuildWithRetry();
            IEnumerable<EmployeeModel> result = null;
            try {
                result = await command.ExecuteAsync(() => person.EmployeeModels(simulate, person), cancellationToken);
            }
            catch (CircuitBreakerOpen e) {
                return NotFound(e.Message);
            }
            catch (Exception) {

            }

            return Ok(new { CommandMetrics = command.Metrics, Data = result });
        }

        [HttpGet("metrics")]
        public IActionResult GetMetrics() {
            return Ok(MetricsCollector.MetricsStore);
        }
    }
}
