using System;
using System.Collections.Generic;
using Stability;
using Stability.Interfaces;
using Stability.Metrics;

namespace Sample.Controllers {
    public static class MetricsCollector {
        public static IList<MetricsModel> MetricsStore = new List<MetricsModel>();
        public static void SubscribeMetrics() {
            CircuitBreakerBuilder.Feed.Subscribe(metrics => MetricsStore.Add(metrics));
        }

        public static ICircuitBreakerBuilder CreateCommand(string commandName) {
            var builder = new CircuitBreakerBuilder()
                .AsKey(commandName)
                .WithMaxFailure(3)
                .WithResetTimeout(TimeSpan.FromSeconds(10))
                .WithMaxRetry(3)
                .RetryInterval(new List<TimeSpan>()
                {
                    TimeSpan.FromMilliseconds(100),
                    TimeSpan.FromMilliseconds(500),
                    TimeSpan.FromMilliseconds(1000)
                });

            return builder;
        }
    }
}