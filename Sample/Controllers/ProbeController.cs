﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sample.Entities;
using Stability;
using Stability.Commands;

namespace Sample.Controllers {
    [Route("api/probe")]
    public class ProbeController : Controller {

        [HttpGet("metrics")]
        public    IActionResult GetCircuitBreakerMetrics() {
            return Ok(CircuitBreakerBuilder.Metrics);
        }

        [HttpGet("success")]
        public IActionResult Get() {
            return Ok(new { Timestamp = DateTime.UtcNow, Message = "Request has been served successfully" });
        }

        [HttpGet("random")]
        public IActionResult GetRandom() {
            var count = DateTime.UtcNow.Second;
            if (count % 2 == 0)
                return Ok($"Success Random Count :{count}");

            return BadRequest($"Service unable to respond random Count :{count}");
        }


        [HttpGet("failed")]
        public IActionResult GetFailed() {
            return BadRequest("Service unable to respond");
        }
    }
}